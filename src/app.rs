use yew::prelude::*;

use crate::components::*;
use crate::dtos::CardDTO;

fn get_default_cards() -> Vec<CardDTO> {
    vec![
        CardDTO::create("1".to_string()),
        CardDTO::create("3".to_string()),
        CardDTO::create("5".to_string()),
    ]
}

pub struct App {
    link: ComponentLink<Self>,
    flipped: bool,
    cards: Vec<CardDTO>,
}

pub enum Msg {
    ToggleFlipped,
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            flipped: false,
            cards: get_default_cards(),
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        true
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::ToggleFlipped => {
                self.flipped = !self.flipped;
                true
            }
        }
    }

    fn view(&self) -> Html {
        html! {
            <main>
                <h2>{"Card:"}</h2>
                <Card score=100 flipped=self.flipped />
                <button onclick=self.link.callback(|_| Msg::ToggleFlipped)>
                    {"Flip it!"}
                </button>
                <CardList cards=self.cards.clone() />
            </main>
        }
    }
}
