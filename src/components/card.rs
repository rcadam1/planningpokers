use yew::prelude::*;

pub struct Card {
    score: u8,
    flipped: bool,
}

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    pub score: u8,
    pub flipped: bool,
}

impl Component for Card {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Card {
            score: props.score,
            flipped: props.flipped,
        }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.flipped = props.flipped;
        self.score = props.score;
        true
    }

    fn view(&self) -> Html {
        let is_flipped = if self.flipped { "yes" } else { "no" };

        html! {
            <div class="card" data-flipped={is_flipped}>
                <div class="card__inner">
                    <div class="card__front">
                        <div class="card__content">
                            <span class="card__corner">{&self.score}</span>
                            <span class="card__score">{&self.score}</span>
                            <span class="card__corner">{&self.score}</span>
                        </div>
                    </div>
                    <div class="card__back">
                        <div class="card__content" />
                    </div>
                </div>
            </div>
        }
    }
}
