use yew::prelude::*;

use crate::dtos::CardDTO;

pub struct CardList {
    cards: Vec<CardDTO>,
}

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    pub cards: Vec<CardDTO>,
}

impl Component for CardList {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { cards: props.cards }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.cards = props.cards;

        true
    }

    fn view(&self) -> Html {
        html! {
          <ul class="card_list">
            {self.cards.iter().map(|card| {
              html!{
                <li class="card_list__item">
                  <div class="card_list__item__background">
                    {card.get_text()}
                  </div>
                  <button class="card_list__item__delete-button">
                    {"X"}
                  </button>
                </li>
              }
            }).collect::<Html>()}
            <li class="card_list__item card_list__new-entry">
              <input
                placeholder="Add a custom entry..."
                class="card_list__new-entry-input"
              />
            </li>
          </ul>
        }
    }
}
