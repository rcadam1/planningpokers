#[derive(Clone, PartialEq)]
pub struct CardDTO {
    text: String,
    enabled: bool,
}

impl CardDTO {
    pub fn get_text(&self) -> String {
        self.text.clone()
    }

    pub fn create(text: String) -> Self {
        Self {
            text,
            enabled: true,
        }
    }
}
